<?php
$export_stamp = date('Ymd_Hisv_T_Z');

// export vote settings.


// export all files in votes/ directory.
$votes_tar = '../exports/export_votes_'.$export_stamp.'.tar.gz';
shell_exec('tar -czvf '.$votes_tar.' ../votes');


// create a metadata file.
$metadata = [
	'vote_system_format' => 1,
	'exported_at' => date('c'),
	'exported_by_ip' => $_SERVER['REMOTE_ADDR'],
	'exported_by_ip_x' => @$_SERVER['HTTP_X_FORWARDED_FOR'],
	'export_settings_shasum' => hash_file('sha1', '../config/votes.json'),
	'export_votes_shasum' => hash_file('sha1', $votes_tar)
];
$metadata_filename = '../exports/export_metadata_'.$export_stamp.'.json';
file_put_contents($metadata_filename, json_encode($metadata, JSON_PRETTY_PRINT));

$master_tar = '../exports/export_master_'.$export_stamp.'.tar.gz';
shell_exec('tar -czvf '.$master_tar.' '.$votes_tar.' '.$metadata_filename.' ../config/votes.json');

shell_exec("rm $metadata_filename $votes_tar");

header('Location: '.$master_tar);
die('Export is klaar. <a href="'.$master_tar.'">Klik om te downloaden</a>');
