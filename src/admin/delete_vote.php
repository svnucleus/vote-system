<?php
$string = file_get_contents("../config/votes.json");
$votes = json_decode($string, true);

$vote_id = $_GET['id'];

if (empty($votes[$vote_id])) {
	die("Stemming bestaat niet.");
}

unset($votes[$vote_id]);
$votes = array_values(array_filter($votes));
$json = json_encode($votes, JSON_PRETTY_PRINT);
file_put_contents('../config/votes.json', $json);
header('Location: /admin/votes.php?removed=1');
