<!--
    Copyright (C) 2018  Stan Overgauw
    Modified by Sander Laarhoven

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<?php
$string = file_get_contents("../config/tokens.json");
$tokens = json_decode($string, true);
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.typekit.net/qvc3lcx.css">
    <link rel="stylesheet" href="/style.css">
    <title>Digitaal stemmen - Admin</title>
</head>
<body>
<nav class="navbar navbar-light nucleus-header">
    <span class="navbar-brand" href="#">
        <img src="https://svnucleus.nl/wp-content/uploads/2019/09/rechthoek-nucleus-logo.png" class="d-inline-block align-top" alt="">
        Digitaal Stemmen &dash; Admin
    </span>
</nav>
<div class="container">
    <div class="row">
        <div class="col">
            <p></p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a href="/admin" class="btn btn-light float-left">Terug naar Overzicht</a>
            <a href="#" class="btn btn-danger btn-generate-tokens float-right">Genereer nieuwe Tokens</a>
            <br><br><br>
            <p>
                Er zijn <?php echo count($tokens); ?> leesbare tokens gegenereerd.
                Deze tokens bevatten de volgende karakters niet: <code>0 O I L 1</code>
                Druk op <code>CTRL</code> + <code>P</code> om deze pagina uit te printen.
            </p>
            <table class="table">
                <?php
                foreach ($tokens as $token){
                    $readable_token = chunk_split($token, 4, ' ');
                    echo "<tr><td class='readable-token large'>$readable_token</td></tr>";
                }
                ?>
            </table>
            <p>
                Dit is het einde van de lijst van <?php echo count($tokens); ?> tokens.
            </p>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<script>
$('.btn-generate-tokens').on('click', function(event) {
    if (!confirm('Genereer nieuwe tokens? Oude tokens worden ongeldig gemaakt.')) {
        return false
    }

    const amount_of_tokens_to_generate = parseInt(prompt('Hoe veel tokens moeten er gemaakt worden?'))

    if (!amount_of_tokens_to_generate || amount_of_tokens_to_generate < 0) {
        alert('Dat is geen goed getal.')
        return false
    }

    document.location = '/admin/gen_tokens.php?amount=' + amount_of_tokens_to_generate
})
</script>
</body>
</html>
