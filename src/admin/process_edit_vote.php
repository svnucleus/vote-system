<!--
    Copyright (C) 2018  Stan Overgauw
    Modified by Sander Laarhoven

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<?php
$string = file_get_contents("../config/votes.json");
$votes = json_decode($string, true);

$svote = $_POST;
$vote_id = $svote['id'];
unset($svote['id']);

function clean_empty_entries($array) {
	foreach ($array as $index => $value) {
		if (empty(trim($array[$index]))) {
			unset($array[$index]);
		}
	}
	return array_values( array_filter($array) );
}

if ($svote['type'] === 'list') {
	unset($svote['options_vertical']);
	unset($svote['options_horizontal']);
	$svote['options'] = clean_empty_entries($svote['options']);
} else {
	unset($svote['options']);
	$svote['options_vertical'] = clean_empty_entries($svote['options_vertical']);
	$svote['options_horizontal'] = clean_empty_entries($svote['options_horizontal']);
}

if (!empty($votes[$vote_id])) {
	$votes[$vote_id] = $svote;
} else {
	$new_vote_id = strtoupper(str_replace(' ', '_', trim($svote['title']) ));
	if ($votes[$new_vote_id]) {
		die("Kies een unieke titel voor de stemming.");
	}
	$votes[$new_vote_id] = $svote;
}

$json = json_encode($votes, JSON_PRETTY_PRINT);
file_put_contents('../config/votes.json', $json);
header('Location: /admin/votes.php?saved=1');
?>
