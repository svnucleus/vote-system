<?php
/**
    Copyright (C) 2018  Stan Overgauw

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
function random_str($length, $keyspace = '23456789ABCDEFGHJKMNPQRSTUVWXYZ')
{
    $pieces = [];
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
        $pieces []= $keyspace[random_int(0, $max)];
    }
    return implode('', $pieces);
}
$list = [];
$amount_of_tokens_to_generate = (int) $_GET['amount'];
if ($amount_of_tokens_to_generate < 0) {
    die("Dat is een te klein getal.");
}
for ($i = 0; $i < $amount_of_tokens_to_generate; $i++) {
    array_push($list, random_str(16));
}
file_put_contents("../config/tokens.json", json_encode($list));
header("Location: tokens.php");
