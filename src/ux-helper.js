
/* shameless copypaste from https://stackoverflow.com/a/29320829/1878974
 	adjusted to also make a-z uppercase. */
document.getElementById('token').addEventListener('input', function (e) {
  	var target = e.target,
	  	position = target.selectionEnd,
		length = target.value.length
  	target.value = target.value.toUpperCase().replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim()
  	target.selectionEnd = position += ((target.value.charAt(position - 1) === ' ' && target.value.charAt(length - 1) === ' ' && length !== target.value.length) ? 1 : 0)
})
